'use strict';

const express = require('express');
const app = express();

const Catalog = require('../models/Catalog');

app.get('/(:catalog_id)?', (req, res, next) => {
  let id = req.params.catalog_id;
  if (id) {
    return Catalog.find(id, (err, catalog) => err ? next(err) : res.send(catalog.data));
  }

  Catalog.findAll((err, catalogs) => err ? next(err) : res.send(catalogs.map(catalog => catalog.data)));
});

app.post('/', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.status(400).send('Empty data');
  }

  let catalog = new Catalog();
  catalog.setData(req.body);
  catalog.save((err, catalog) => err ? next(err) : res.send(catalog.data));
});

app.put('/:catalog_id', (req, res, next) => {
  if (!Object.keys(req.body).length) {
    return res.status(400).send('Empty data');
  }

  let id = req.params.catalog_id;

  Catalog.find(id, (err, catalog) => {
    catalog.setData(req.body);
    catalog.save((err, catalog) => err ? next(err) : res.send(catalog.data));
  });
});

app.delete('/:catalog_id', (req, res, next) => {
  let id = req.params.catalog_id;
  if (!catalogs[id]) {
    return res.status(404).send('Catalog not found');
  }

  Catalog.find(id, (err, catalog) => {
    catalog.delete(err => err ? next(err) : res.send());
  });
});

module.exports = app;
