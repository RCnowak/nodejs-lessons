'use strict';

var Mongodb = require('mongodb-custom');

class Product {
  constructor() {
    this.isNew = true;
    this.data = {};
  }

  static findAll(cb) {
    Mongodb.findAll('products', (err, docs) => {
      if (err) { return cb(err); }

      let products = docs.map(doc => {
        let product = new Product();
        product.isNew = false;
        return product.setData(doc);
      });

      cb(err, products);
    });
  }

  static find(id, cb) {
    Mongodb.find('products', id, (err, doc) => {
      if (err) { return cb(err); }

      let product = new Product();
      product.isNew = false;
      product.setData(doc);

      cb(err, product);
    });
  }

  setData(data) {
    Object.keys(data).map(k => this.data[k] = data[k]);
    return this;
  }

  save(cb) {
    let isSave = (err, id) => {
      if (err) { return cb(err); }
      Product.find(id, (err, product) => {
        this.data = product.data;
        this.isNew = product.isNew;
        cb(err, this);
      });
    };

    if (this.isNew) {
      Mongodb.insert('products', this.data, isSave);
    } else {
      Mongodb.update('products', this.data._id, this.data, isSave);
    }
  }

  delete(cb) {
    if (this.isNew) {
      cb(new Error('Can\'t be delete'));
    } else {
      Mongodb.update('products', this.data._id, this.data, cb);
    }
  }
}

module.exports = Product;
